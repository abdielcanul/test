import 'dart:convert';

FrasesModel fraseFromJson(String str) => FrasesModel.fromJson(json.decode(str));
String fraseModelToJson(FrasesModel data) => json.encode(data.toJson());

class FrasesModel {
  int? id;
  String? name;
  String? descripcion;
  String? img;

  FrasesModel({
    this.id,
    this.descripcion,
    this.img,
    this.name,
  });

  factory FrasesModel.fromJson(Map<String, dynamic> json) => FrasesModel(
        id: json["id"],
        descripcion: json["description"],
        img: json["images"][0][0],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "description": descripcion,
        "images" "0" "0": img,
        "name": name,
      };
}

List<FrasesModel> frasesModelsFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<FrasesModel>.from(str.map((item) {
    return FrasesModel.fromJson(item);
  }));
}

String frasesModelsToJson(FrasesModel frasesModel) {
  final dyn = frasesModel.toJson();
  return json.encode(dyn);
}
