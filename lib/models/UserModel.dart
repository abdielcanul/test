class Users {
  String? name;
  String? lastname;
  String? email;
  String? phone;
  String? password;

  Users({
    this.name,
    this.lastname,
    this.email,
    this.phone,
    this.password,
  });

  Users.fromMap(Map<String, dynamic> map) {
    name = map['name'];
    lastname = map["lastname"];
    email = map["email"];
    phone = map["phone"];
    password = map["password"];
  }

  Map<String, dynamic> toMap() {
    return {
      "name": name,
      "lastname": lastname,
      "email": email,
      "phone": phone,
      "password": password,
    };
  }
}
