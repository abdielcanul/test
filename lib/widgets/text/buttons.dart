// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';

Widget buttonGenerico(
  bool? hayVaciosTextForm,
  Function validarExistenciaEInsertar,
  String titulo,
) =>
    Container(
      padding: EdgeInsets.only(left: 30.0, right: 30.0, bottom: 6.0),
      width: double.infinity,
      // ignore: deprecated_member_use
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () async {
          validarExistenciaEInsertar();
        },
        padding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.blue.shade900,
        child: Text(
          titulo,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
