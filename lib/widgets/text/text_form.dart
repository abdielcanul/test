import 'package:flutter/material.dart';

bool passwordVisible = true;

Widget textFieldIcon(
        // Function onChanged,
        BoxDecoration decoration,
        TextStyle styleText,
        TextEditingController controller,
        String label,
        double width,
        bool isPassword) =>
    Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(label, style: styleText),
            const SizedBox(
              height: 2,
            ),
            Container(
              alignment: Alignment.centerLeft,
              decoration: decoration,
              padding: const EdgeInsets.only(left: 10),
              child: TextFormField(
                controller: controller,
                //onChanged: (value) => onChanged(value),
                style: styleText,
                decoration: InputDecoration(
                  hintText: label,
                  border: InputBorder.none,
                ),
                obscureText: isPassword ? passwordVisible : false,
              ),
            ),
          ],
        ));
