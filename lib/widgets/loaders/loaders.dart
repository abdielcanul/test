import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class LoadingPrincipalWidget extends StatefulWidget {
  const LoadingPrincipalWidget({Key? key}) : super(key: key);

  @override
  _LoadingPrincipalWidgetState createState() => _LoadingPrincipalWidgetState();
}

class _LoadingPrincipalWidgetState extends State<LoadingPrincipalWidget> {
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Card(
      color: Colors.transparent,
      elevation: 0,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(32.0),
            child: Image.asset("assets/corcholata.gif"),
          )
        ],
      ),
    );
  }
}

loadercocacola(BuildContext context) {
  AwesomeDialog(
    dialogBackgroundColor: Colors.transparent,
    dismissOnBackKeyPress: false,
    dismissOnTouchOutside: false,
    isDense: false,
    context: context,
    dialogType: DialogType.NO_HEADER,
    animType: AnimType.BOTTOMSLIDE,
    body: LoadingPrincipalWidget(),
  )..show();
}

class LoadingWidget extends StatefulWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  _LoadingWidgetState createState() => _LoadingWidgetState();
}

class _LoadingWidgetState extends State<LoadingWidget> {
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      color: Colors.transparent,
      width: MediaQuery.of(context).size.width * .5,
      height: MediaQuery.of(context).size.width * .5,
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Image.asset("assets/corcholata.gif"),
      ),
    );
  }
}
