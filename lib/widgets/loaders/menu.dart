import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:frase_del_dia/page/home.dart';
import 'package:frase_del_dia/widgets/loaders/lostconexion.dart';

import 'package:get/get.dart';

void main() => runApp(MenuPrincipal());

class MenuPrincipal extends StatefulWidget {
  const MenuPrincipal({Key? key}) : super(key: key);

  @override
  _MenuPrincipalState createState() => _MenuPrincipalState();
}

int index = 0;

class _MenuPrincipalState extends State<MenuPrincipal> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FloatingNavbar(
      elevation: 0,
      backgroundColor: Colors.white,
      borderRadius: 26,
      selectedItemColor: Colors.blue.shade900,
      unselectedItemColor: Colors.black,
      currentIndex: index,
      onTap: (int val) {
        //setState(() {
        switch (val) {
          case 0:
            //navegamos al home.
            print("home");
            Get.off(HomePage());
            break;
          // case 1:
          //   //navegamos al detallista
          //   Navigator.popAndPushNamed(context, "/retaillers");
          //   break;
          case 1:
            //navegamos al detallista
            alertLostConexion(context);
            // Navigator.popAndPushNamed(context, "/login");
            break;
        }
        //});
        setState(() => index = val);
      },
      items: [
        FloatingNavbarItem(
          icon: Icons.home,
          title: 'Inicio',
        ),
        // FloatingNavbarItem(
        //   icon: Icons.favorite,
        //   title: 'Favoritos',
        // ),
        FloatingNavbarItem(
          icon: Icons.exit_to_app,
          title: 'Salir',
        ),
      ],
    );
  }
}
