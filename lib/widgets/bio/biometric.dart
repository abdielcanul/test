import 'package:flutter/material.dart';
import 'package:frase_del_dia/page/login.dart';

Widget biometric(
  IconData icon,
  Function bio,
) =>
    // ignore: avoid_unnecessary_containers
    Container(
        child: IconButton(
      icon: Icon(
        icon,
        size: 70,
      ),
      onPressed: () {
        bio;
      },
    ));
