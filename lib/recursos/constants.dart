import 'package:flutter/material.dart';

final kBoxDecorationStyle = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(40.0),
    border: Border.all(color: Colors.grey));

final kBoxDecorationStyle2 = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(27.0),
    border: Border.all(color: Colors.black));

final kHintTextStyle = TextStyle(color: Colors.black, fontFamily: 'OpenSans');
