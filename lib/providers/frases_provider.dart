import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:frase_del_dia/models/frasesRequestModel.dart';
import 'package:frase_del_dia/recursos/variables_globales.dart';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>?> getFrases() async {
  print(URL_BASE);
  try {
    final response = await http.get(Uri.https("$URL_BASE", "/qod"), headers: {
      "content-type": "application/json",
      "Accept": "application/json"
    });
    var a = json.decode(response.body);
    //borrar este print
    print("${a.toString()} ====> ");

    print(response.statusCode.toString());

    return a;
  } catch (e) {
    print("$e ############>>>>");
  }
}
