//Se guardara el id, nombre, apellido , correo y numero de telefono
import 'package:frase_del_dia/models/UserModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:developer';
import 'dart:io';

class DataBaseInterna {
  Database? database;
  String nameDataBase = "FRASEDELDIA.db";
  DataBaseInterna._();
  static final DataBaseInterna db = DataBaseInterna._();

  Future<Database> get database2 async {
    if (database != null) {
      return database!;
    }
    database = await getDataBaseInstanace();
    return database!;
  }

  Future<Database> getDataBaseInstanace() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(
        directory.path, nameDataBase); // le asignamos nombre ala base de datos
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      //TODO: Estas son las tablas que se rellenaran con los datos de los providers
      await db.execute("CREATE TABLE " +
          "Users" +
          " (" //ejecutamos el query para crear la tabla con sus atributos

              "name TEXT,"
              "lastname TEXT,"
              "email TEXT,"
              "phone TEXT,"
              "password TEXT"
              ")");
    });
  }

  Future<void> insertUsers(Users users) async {
    Database? db = await database2;
    db.insert("Users", users.toMap());
  }

  Future<List<Users>> validarExistenciaUsuario(String correo) async {
    Database? db = await database;
    List<Map<String, dynamic>> results =
        await db!.query("Users", where: "email = ?", whereArgs: [correo]);
    print(results);
    return results.map((e) => Users.fromMap(e)).toList();
  }
}
