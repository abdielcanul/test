import 'package:flutter/material.dart';
import 'package:frase_del_dia/Database/data_base.dart';
import 'package:frase_del_dia/models/UserModel.dart';
import 'package:frase_del_dia/page/home.dart';
import 'package:frase_del_dia/page/login.dart';
import 'package:frase_del_dia/page/onboarding.dart';
import 'package:frase_del_dia/page/splashscreen.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    sedder();
  }

  final fido = Users(
    name: "Juan",
    phone: "999999999",
    email: "sedder@gmail.com",
    lastname: "Perez",
    password: "111111111",
  );

  sedder() async {
    await DataBaseInterna.db.insertUsers(fido);
  }

  @override
  Widget build(BuildContext context) {
    String route = '/login';

    return GetMaterialApp(
      locale: Locale("es", "ES"),
      // ignore: prefer_const_literals_to_create_immutables
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      // ignore: prefer_const_literals_to_create_immutables
      supportedLocales: [
        const Locale('es', 'ES'),
        const Locale('en', 'US'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Frase del día',
      theme: ThemeData(
        fontFamily: 'Poppins',
      ),
      getPages: [
        GetPage(
            name: "/",
            page: () => SplashScreen(
                  route: route,
                )),
        GetPage(name: "/login", page: () => LoginPage()),
        GetPage(name: '/home', page: () => HomePage()),
        GetPage(name: '/onboarding', page: () => OnBoarding()),
      ],
    );
  }
}
