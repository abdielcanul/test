// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:frase_del_dia/page/home.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';

bool pasoOnboarding = false;

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  final introKey = GlobalKey<IntroductionScreenState>();
  final controller = ScrollController();
  int index = 0;

  @override
  void initState() {
    super.initState();
    index = 0;
    //prefs.verIntroduccion = true;
    // print("se actualizo las bases de datos $sinconexion");
    setState(() {
      pasoOnboarding = true;
    });
  }

  final pageDecoration = PageDecoration(
    titleTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 20.0,
      height: 1.5,
    ),
    bodyTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 18.0,
      height: 1.2,
    ),
    contentMargin: const EdgeInsets.all(10),
  );

  List<PageViewModel> getPages() {
    return [
      PageViewModel(
        image: Padding(
          padding: const EdgeInsets.only(top: 70.0),
          child: Lottie.asset(
            'assets/50124-user-profile.json',
            height: 300,
          ),
        ),
        title: "Autenticación con datos biometricos",
        bodyWidget: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Text(
                "Para una mejor experiencia de usuario la aplicación cuenta con acceso con datos biometricos",
                style: TextStyle(color: Colors.white, fontSize: 17),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),

            //boton de siguiente
          ],
        ),
        decoration: PageDecoration(
          titleTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            height: 1.5,
          ),
          bodyTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
            height: 1.2,
          ),
          contentMargin: const EdgeInsets.all(10),
        ),
      ),
      PageViewModel(
        image: Padding(
          padding: const EdgeInsets.only(top: 70.0),
          child: Lottie.asset(
            'assets/65237-relaxed-woman-meditating.json',
            height: 300,
          ),
        ),
        title: "Frases motivadoras",
        bodyWidget: Column(
          children: [
            Text(
              "La app contiene frases motivadoras, para ayudarte en tu día a día",
              style: TextStyle(color: Colors.white, fontSize: 17),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          ],
        ),
        decoration: PageDecoration(
          titleTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            height: 1.5,
          ),
          bodyTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
            height: 1.2,
          ),
          contentMargin: const EdgeInsets.all(10),
        ),
      ),
      PageViewModel(
        image: Padding(
          padding: const EdgeInsets.only(top: 70.0),
          child: Lottie.asset(
            'assets/26355-free-time.json',
            height: 300,
          ),
        ),
        title: "Tiempo libre",
        bodyWidget: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 27.0),
              child: Text(
                "La app puedes usarla en tu tiempo libre",
                style: TextStyle(color: Colors.white, fontSize: 17),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide(color: Colors.red)))),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 46.0, right: 46, top: 8, bottom: 8),
                child: Text(
                  "Comenzar",
                  style: TextStyle(color: Colors.red, fontSize: 20),
                ),
              ),
            ),
          ],
        ),
        decoration: PageDecoration(
          titleTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            height: 1.5,
          ),
          bodyTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
            height: 1.2,
          ),
          contentMargin: const EdgeInsets.all(10),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: IntroductionScreen(
        globalBackgroundColor: Colors.blue.shade900,
        dotsDecorator: DotsDecorator(
            color: Colors.grey,
            activeColor: Colors.white,
            size: const Size.square(9.0),
            activeSize: const Size(18.0, 9.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0))),
        pages: getPages(),
        scrollController: controller,
        initialPage: index,
        globalHeader: GestureDetector(
          child: Container(
            alignment: Alignment.centerRight,
            width: double.infinity,
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * .06, right: 16),
            child: const Text(
              "Saltar",
              style: TextStyle(
                  color: Colors.white,
                  decoration: TextDecoration.underline,
                  fontSize: 16,
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.w500),
            ),
          ),
          onTap: () {
            setState(() {
              pasoOnboarding = true;
            });

            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          },
        ),
        showNextButton: false,
        showDoneButton: false,
        done: Text(
          'Comenzar',
          style: TextStyle(
            color: Colors.white,
            fontSize: size.width * 0.042,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
