// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:frase_del_dia/Database/data_base.dart';
import 'package:frase_del_dia/models/UserModel.dart';
import 'package:frase_del_dia/recursos/constants.dart';
import 'package:frase_del_dia/widgets/text/buttons.dart';
import 'package:frase_del_dia/widgets/text/text_form.dart';
import 'package:lottie/lottie.dart';
import 'package:encrypt/encrypt.dart' as enc;

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  enc.Encrypter? encrypter;
  enc.Key? key;
  enc.IV? iv;
  //ojo para visualizar la contraseña
  bool passwordVisible = true;
  bool? camposVacios;
  TextEditingController nombre = TextEditingController();

  TextEditingController telefono = TextEditingController();
  TextEditingController apellido = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    key = enc.Key.fromLength(32);
    iv = enc.IV.fromLength(8);
    encrypter = enc.Encrypter(enc.Salsa20(key!));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Lottie.asset(
                'assets/31548-robot-says-hello.json',
                height: 200,
              ),
            ),
            Text(
              "Hola!!! \nRegistrarte para acceder a esta increible app !!!",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 15),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textFieldIcon(kBoxDecorationStyle, kHintTextStyle, nombre,
                  "Nombre", 370, false),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textFieldIcon(kBoxDecorationStyle, kHintTextStyle,
                  apellido, "Apellido", 370, false),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textFieldIcon(kBoxDecorationStyle, kHintTextStyle,
                  telefono, "Número de telefono", 370, false),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textFieldIcon(kBoxDecorationStyle, kHintTextStyle, email,
                  "Correo electronico", 370, false),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textFieldIcon(kBoxDecorationStyle, kHintTextStyle,
                  password, "Contraseña", 370, true),
            ),
            // _confirmpassword(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: buttonGenerico(camposVacios, savedRegister, "Registrar"),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> savedRegister() async {
    if (nombre.text != "" &&
        telefono.text != "" &&
        apellido.text != "" &&
        email.text != "" &&
        password.text != "") {
      final fido = Users(
        name: nombre.text,
        phone: telefono.text,
        email: email.text,
        lastname: apellido.text,
        password: password.text,
      );

      await DataBaseInterna.db
          .validarExistenciaUsuario(email.text)
          .then((value) async {
        if (value.isNotEmpty) {
          print(value.elementAt(0).email);
          Fluttertoast.showToast(
            msg: "El correo electronico ya ha sido registrado",
            timeInSecForIosWeb: 2,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            webShowClose: true,
          );
        } else {
          await DataBaseInterna.db.insertUsers(fido);
          Fluttertoast.showToast(
            msg: "Se ha registrado el usuario con Exito !!!",
            timeInSecForIosWeb: 2,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            webShowClose: true,
          );
          // Navigator.popAndPushNamed(context, "/onboarding");
        }
      });
    } else {
      Fluttertoast.showToast(
        msg: "No has escrito nada",
        timeInSecForIosWeb: 2,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        webShowClose: true,
      );
    }
  }
}
