// ignore_for_file: prefer_const_constructors
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:frase_del_dia/Database/data_base.dart';
import 'package:frase_del_dia/page/register.dart';
import 'package:frase_del_dia/widgets/bio/biometric.dart';
import 'package:frase_del_dia/widgets/text/buttons.dart';
import 'package:frase_del_dia/widgets/text/text_form.dart';
import 'package:local_auth/local_auth.dart';
import 'package:frase_del_dia/recursos/constants.dart';
import 'package:encrypt/encrypt.dart' as enc;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

bool isBiometricAvailable = false;

class _LoginPageState extends State<LoginPage> {
  //VARIABLES
  LocalAuthentication? _localAuthentication;

  //controladores para los labels
  TextEditingController telefono = TextEditingController();
  TextEditingController password = TextEditingController();

  //ojo para visualizar la contraseña
  bool passwordVisible = true;
  enc.Encrypter? encrypter;
  enc.Key? key;
  enc.IV? iv;
  bool? camposVacios;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    key = enc.Key.fromLength(32);
    iv = enc.IV.fromLength(8);

    _localAuthentication = LocalAuthentication();
    _localAuthentication!.canCheckBiometrics.then((value) {
      setState(() {
        isBiometricAvailable = value;
      });
      print(isBiometricAvailable);
    });
  }

  //BODY DE LA PAGINA

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade900,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ),
                    margin: EdgeInsets.all(15),
                    elevation: 10,
                    child: Container(
                      padding: EdgeInsets.all(0),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 60,
                          ),
                          Text(
                            'Login APP',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Que bueno verte de nuevo',
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 10.0, right: 10),
                            child: textFieldIcon(
                              kBoxDecorationStyle,
                              kHintTextStyle,
                              telefono,
                              "Tu correo electronico",
                              300,
                              false,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10.0, right: 10, top: 30),
                            child: textFieldIcon(
                              kBoxDecorationStyle,
                              kHintTextStyle,
                              password,
                              "Tu contraseña",
                              300,
                              true,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: GestureDetector(
                              onTap: () async {
                                if (isBiometricAvailable) {
                                  bool didAuthenticate =
                                      await _localAuthentication!
                                          // ignore: deprecated_member_use
                                          .authenticateWithBiometrics(
                                              localizedReason:
                                                  "Por favor identificate");
                                  if (didAuthenticate) {
                                    Navigator.popAndPushNamed(
                                        context, "/onboarding");
                                    log("Te autenticaste correctamente");
                                  } else {
                                    log("No te autenticaste");
                                  }
                                } else {
                                  print("aqui cae");
                                }
                              },
                              child: Icon(
                                Icons.fingerprint,
                                size: 80,
                              ),
                            ),
                          ),
                          //biometric(Icons.fingerprint, biometricMethod),
                          // ignore: prefer_const_constructors
                          SizedBox(
                            height: 20,
                          ),
                          //aqui va boton de iniciar sesion
                          buttonGenerico(camposVacios, saved, "Iniciar sesión"),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            height: 30,
                            color: Colors.grey,
                            thickness: 1,
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegisterPage()),
                              );
                            },
                            child: Text(
                              'Registrarme',
                              style: TextStyle(
                                color: Colors.blue.shade900,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //METODOS

  validarCampos() {
    if (telefono.text != "" && password.text != "") {
      setState(() {
        camposVacios = false;
      });
    } else {
      setState(() {
        camposVacios = true;
      });
    }
  }

  Future<void> biometricMethod() async {
    print("entro al metodo biometric");
    if (isBiometricAvailable) {
      bool didAuthenticate = await _localAuthentication!
          // ignore: deprecated_member_use
          .authenticateWithBiometrics(
              localizedReason: "Por favor identificate");
      if (didAuthenticate) {
        Navigator.popAndPushNamed(context, "/onboarding");
        log("Te autenticaste correctamente");
      } else {
        log("No te autenticaste");
      }
    } else {
      print("aqui cae");
    }
  }

  Future<void> saved() async {
    //validarCampos();
    if (telefono.text != "" && password.text != "") {
      await DataBaseInterna.db
          .validarExistenciaUsuario(telefono.text)
          .then((value) async {
        print(value);
        if (value.isNotEmpty) {
          print(value.elementAt(0).password);
          if (telefono.text == value.elementAt(0).email &&
              password.text == value.elementAt(0).password) {
            Navigator.popAndPushNamed(context, "/onboarding");
            Fluttertoast.showToast(
              msg: "Inicio de sesión con exito",
              timeInSecForIosWeb: 2,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              webShowClose: true,
            );
          } else {
            Fluttertoast.showToast(
              msg: "Las credenciales son incorrectas",
              timeInSecForIosWeb: 2,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.TOP,
              webShowClose: true,
            );
          }
        } else {
          Fluttertoast.showToast(
            msg: "No estas registrado",
            timeInSecForIosWeb: 2,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.TOP,
            webShowClose: true,
          );
        }
        print(value);
      });
    } else {
      Fluttertoast.showToast(
        msg: "No haz escrito nada",
        timeInSecForIosWeb: 2,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        webShowClose: true,
      );
    }
  }
}
