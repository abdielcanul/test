// ignore_for_file: prefer_const_constructors
import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:frase_del_dia/providers/frases_provider.dart';
import 'package:frase_del_dia/widgets/loaders/menu.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math' as math;

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String author = "";
  String title = "";
  String quote = "";
  String category = "";
  String date = "";

  bool? haydatos;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFrasesMethod();
  }

  getFrasesMethod() async {
    await getFrases().then((value) {
      log(value.toString());
      try {
        setState(() {
          author = value!["contents"]["quotes"][0]["author"].toString();
          title = value["contents"]["quotes"][0]["title"].toString();
          quote = value["contents"]["quotes"][0]["quote"].toString();
          category = value["contents"]["quotes"][0]["category"].toString();
          date = value["contents"]["quotes"][0]["date"].toString();
        });
        if (author != null &&
            title != null &&
            quote != null &&
            category != null &&
            date != null) {
          setState(() {
            haydatos = true;
          });
        } else {
          setState(() {
            haydatos = false;
          });
        }
      } catch (e) {
        print("$e ======>");
      }
    }).onError((error, stackTrace) {
      //log("$error ======>");
    });

    print(author);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
          .withOpacity(1.0),
      body: haydatos == true
          ? Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 35.0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: 60,
                          child: Image.asset(
                            "assets/icons8-cita-izquierda-90.png",
                          ),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 18.0),
                    child: SizedBox(
                        width: 300,
                        child: Text(
                          quote.toString(),
                          style: TextStyle(
                              fontSize: 20, fontStyle: FontStyle.italic),
                        )),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 55.0, top: 20, bottom: 100),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          author.toString(),
                          style: TextStyle(
                              fontSize: 19, fontWeight: FontWeight.bold),
                        )),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () async {
                            await FlutterShare.share(
                              title:
                                  "Hola te comparto una frase que me encanto !!!",
                              text: '${quote} \n${author}',
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(width: 1, color: Colors.black)),
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.share,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          : Center(child: CircularProgressIndicator()),
      bottomNavigationBar: MenuPrincipal(),
    );
  }
}
