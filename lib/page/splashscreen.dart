import 'dart:async';

import 'package:flutter/material.dart';
import 'package:frase_del_dia/page/home.dart';
import 'package:frase_del_dia/page/login.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key, this.route}) : super(key: key);
  final String? route;
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  //metodo para cambiar de pantalla por un tiempo determinado
  changeView() async {
    Timer(Duration(milliseconds: 1500), () async {
      if (widget.route == '/login') {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
        //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => PageExample()));
      } else
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(milliseconds: 500), () => changeView());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset('assets/35184-book-with-bookmark.json'),
            Text(
              "Frase del día APP",
              style: TextStyle(fontSize: 30),
            ),
          ],
        ),
      ),
    );
  }
}
